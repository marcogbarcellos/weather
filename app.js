// app.js
var koa = require('koa');
var request = require('koa-request');
var bodyParser = require('koa-body-parser');
var app = koa();
var common = require('koa-common');
var lat, lon, weatherMain, weatherCode, weatherDescription, 
	temp, tempMax, tempMin, pressure, humidity, seaLevel;

 
app.use(bodyParser()); 
// enable logger middleware
app.use(common.logger('dev'));
 
// enable static middleware
app.use(common.static(__dirname + '/public'));

app.use(function *middleware1(next) {
    console.log('middleware1 start');
    /*var options = {
	  host: 'http://jsonip.com/',
	  method: 'GET'
	};*/

	var options = {
        url: 'http://ip-api.com/json',
        headers: { 'User-Agent': 'request' }
    };
 
    var response = yield request(options); 
 	var data = JSON.parse(response.body);

 	lat = data.lat;
 	lon = data.lon;
    console.log('your lon is ' + lon);
    console.log('your lat is ' + lat);
    yield next;
    console.log('middleware1 end');
});
 
app.use(function *middleware2(next) {
    console.log('middleware2 start');
    //
    var options = {
        url: 'http://api.openweathermap.org/data/2.5/weather?lat='+lat+'&lon='+lon+'&appid=c72b6a3232aa058c8c2055609b24f93f',
        headers: { 'User-Agent': 'request' }
    };
 
    var response = yield request(options); 
 	var data = JSON.parse(response.body);

 	weatherMain = data.weather[0].main;
 	weatherCode = data.weather[0].id;
 	weatherDescription = data.weather[0].description;
 	temp = data.main.temp;
 	tempMin = data.main.temp_min;
 	tempMax = data.main.temp_max;
 	pressure = data.main.pressure;
 	humidity = data.main.humidity;
 	seaLevel = data.main.sea_level;
    yield next;
    console.log('middleware2 end');
});

/*app.get('http://jsonip.com/',getit);

function *getit(){
 console.log(this.req.body); //undefined
}*/
 
app.use(function *() {
    this.body = "Weather: "+weatherMain+"\nWeather Code:"+weatherCode
			    +"\nWeather Description:"+weatherDescription
			    +"\nTemperature:"+temp+"\nMax Temperature:"+tempMax
			    +"\nMin Temperature:"+tempMin+"\nPressure:"+pressure
			    +"\nHumidity:"+humidity+"\nSea Level:"+seaLevel;
});

 
var server = app.listen(4000, function () {
    console.log('Listening on port %d', server.address().port);
});